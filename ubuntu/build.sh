#!/bin/bash

source ./settings

if [ ! -f ./assets/id_rsa ]; then
        echo "Please generate an SSH key for this slave to use and place it at ./assets/id_rsa"
        exit 1
fi

# Build base image
docker build -t ${IMAGE_NAME} .

for i in $(seq -w 01 $INSTANCES); do
	# Cleanup old instances
	docker stop ${IMAGE_NAME}-$i 2>/dev/null
	docker rm ${IMAGE_NAME}-$i 2>/dev/null
	
	# First start, we configure the buildbot environment
	docker run -d --env BUILDBOT_MASTER_HOST=${MASTERNAME} \
		--env BUILDSLAVE_NAME=${SLAVENAME}$i \
		--env BUILDSLAVE_KEY=${SLAVEKEY} \
		--env BUILDSLAVE_OWNER="${OWNER}" \
		--env BUILDSLAVE_INFO="${INFO}" \
		--name ${IMAGE_NAME}-$i -t ${IMAGE_NAME} /bin/bash /boot.sh

	sleep 10
	
	# Stop instances
	docker stop ${IMAGE_NAME}-$i

	echo "Container ${IMAGE_NAME}-$i created containing buildbot slave ${SLAVENAME}$i."
done
