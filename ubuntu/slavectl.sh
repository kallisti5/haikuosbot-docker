#!/bin/bash

source ./settings

start() {
	echo 'Starting buildslaves...'
	for i in $( seq -w 01 $INSTANCES ); do
		docker start ${IMAGE_NAME}-$i
		sleep 5 
		RUNNING=$(docker ps | grep ${IMAGE_NAME}-$i | wc -l)
		if [ $(docker ps | grep ${IMAGE_NAME}-$i | wc -l) -lt 1 ]; then
			echo "Error starting ${IMAGE_NAME}-$i"
		fi
	done
}


stop() {
	echo 'Stopping buildslaves...'
	for i in $( seq -w 01 $INSTANCES ); do
		docker stop ${IMAGE_NAME}-$i
		sleep 2
	done
}


status() {
	docker ps
}

case "$1" in
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		stop
		start
		;;
	status)
		status
		;;
	*)
		echo "Usage: $0 {start|stop|restart|status}"
		;;
esac
