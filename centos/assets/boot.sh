#!/bin/bash

BBSLAVE_REPO=/var/lib/buildbot/slaves
BBSLAVE=haiku

# If this is the first run, create buildbot slave
if [ ! -d $BBSLAVE_REPO/$BBSLAVE ]; then
	mkdir -p $BBSLAVE_REPO
	buildslave create-slave $BBSLAVE_REPO/$BBSLAVE $BUILDBOT_MASTER_HOST $BUILDSLAVE_NAME $BUILDSLAVE_KEY
	echo "${BUILDSLAVE_OWNER}" > $BBSLAVE_REPO/$BBSLAVE/info/admin
	echo "${BUILDSLAVE_INFO}" > $BBSLAVE_REPO/$BBSLAVE/info/host
else
	# Hide the password from the buildslave environment
	unset BUILDSLAVE_KEY
	unset BUILDSLAVE_OWNER
	unset BUILDSLAVE_INFO

	buildslave start --nodaemon $BBSLAVE_REPO/$BBSLAVE
fi
