Haiku buildbot Docker containers
=========

These dockerfiles make up the [Haiku] buildbot environment. Each folder contains
the Dockerfile definition to spawn an OS builder based on the described Linux filesystem.

[Docker] is an open-source project that automates the deployment of applications inside software containers.

> The Linux kernel provides cgroups for resource isolation (CPU, memory, block I/O, network, etc.) that do not require starting any virtual machines. The kernel also provides namespaces to completely isolate an application's view of the operating environment, including process trees, network, user ids and mounted file systems. LXC (LinuX Containers) combines cgroups and namespace support to provide an isolated environment for applications; Docker is built on top of LXC, enabling image management and deployment services.

Requirements
--------------

* A linux system which with docker installed or a server running [CoreOS]
* A stable internet connection
* 2GB+ of RAM
 
Installation
--------------

* Edit the settings file for your environment / buildslave / etc
* Execute build.sh in each environment to build the docker container
* Execute slavectl.sh start to start each environment

Shell
--------------

You can enter the running container by using the shell.sh script included.
Just provide the container name from 'docker ps'

[Haiku]:http://haiku-os.org
[Docker]:http://docker.com
[CoreOS]:http://coreos.com
