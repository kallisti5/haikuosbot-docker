#!/bin/bash

# Usage: shell.sh <container name>

PID=$(docker inspect --format '{{.State.Pid}}' $1)
sudo nsenter --target $PID --mount --uts --ipc --net --pid
